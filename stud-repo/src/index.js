//Step 2: change the base code to use Component
import React, { Component } from 'react'
import Web3 from 'web3'
import './App.css';
//Step 1: Import the Bootstrap CSS to replace index
import 'bootstrap/dist/css/bootstrap.css';//We replace function with a component for better management of code
class App extends Component {
  //componentDidMount called once on the client after the initial render
  //when the client receives data from the server and before the data is displayed.
  componentDidMount() {
    if (!window.ethereum)
      throw new Error("No crypto wallet found. Please install it.");
    window.ethereum.send("eth_requestAccounts");
    this.loadBlockchainData()
  }
  //create a web3 connection to using a provider, MetaMask to connect to
  //the ganache server and retrieve the first account
  async loadBlockchainData() {
    const web3 = new Web3(Web3.givenProvider || "http://localhost:7545")
    const accounts = await web3.eth.getAccounts()
    this.setState({ account: accounts[0] })
  }
  //Initialise the variables stored in the state
  constructor(props) {
    super(props)
    this.state = {
      account: '',
      studentCount: 0,
      students: []
    }
  }
  //Display the first account
  render() {
    return (
      <div className="container">
      <h1>Hello, World!</h1>
      <p>Your account: {this.state.account}</p>
      </div>
    );
  }
}
export default App;
  

